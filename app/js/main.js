/*
* 
*   Check IE browser
*   It add class .browser-IE for <html>
*   Some elements don't display c correctly in IE. That class fix it.
*
* */
if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i)) {
    $("html").addClass("browser-IE");
}


$(function () {

    /*
    *
    *   Toggle fixed menu on mobile screen
    *
    * */
    $('[data-action="toggle-menu"]').on('click', function (e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('body').toggleClass('overflow-locked');
        $(target).toggleClass('show');
    });

    /*
    *
    *   Init video modal plugin ( Youtube/Vimeo )
    *
    * */
    $('.js-modal-btn').modalVideo();

    /*
    *
    *   Feedback slider initialize
    *
    * */
    var fs = $('#feedback-slider');
    fs.owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        dots: false
    });

    /*
    *
    *   Custom slider navigation event handler
    *
    * */
    $('.slide-arrows [rel]').on('click', function () {
        var target = $(this).data('slider');
        var direction = $(this).attr('rel');
        $(target).trigger(direction + '.owl.carousel');
    });

    /*
    * 
    *   Parallax - for home screens
    *   https://github.com/wagerfield/parallax
    * 
    * */
    var scene = document.getElementById('scene');
    if (scene) {
        var parallax = new Parallax(scene, {
            selector: '.layer',
            frictionX: '0',
            frictionY: '0.15'
        });
    }

    /*
    * 
    *   AOS - Animate on scroll library
    *   https://github.com/michalsnik/aos
    * 
    * */
    AOS.init({
        offset: 50,
        duration: 1000,
        once: true
    });

    /*
    * 
    *  Check input value for prevent
    *  wrong label position
    * 
    * */
    $('.form-control').each(function () {
        if (!$(this).val()) {
            $(this).removeClass('empty');
        } else {
            $(this).addClass('empty');
        }
    });

    $('.form-control').on('blur', function () {
        if (!$(this).val()) {
            $(this).removeClass('empty');
        } else {
            $(this).addClass('empty');
        }
    });

    /*
    * 
    *   Validate form
    * 
    * */
    var contactForm = $('#contactForm').validate({
        submitHandler: function(form){
            $.ajax({
                type: 'POST',
                url: 'send_mail.php',
                data: $(form).serialize(),
                success: function (data) {
                    $('#formSuccessAlert').slideDown();
                    setTimeout(function(){
                        $('#formSuccessAlert').slideUp();
                    }, 3000 );

                    $('#contactForm input:not([type="submit"]), #contactForm textarea').val('').removeClass('valid empty');
                },
                error: function (xhr, str) {
                    console.error(str);
                }
            });
        }
    });



    /*
    * 
    *   Contact page map scripts
    * 
    * */
    if ($('#mapOffices').length != 0) {

        /* map options */
        var mapOptions = {
            zoom: 12,
            center: {lat: 40.715536, lng: -73.954927},
            scrollwheel: true,
            mapTypeControl: false,
            disableDefaultUI: true
        };

        /* initialize map */
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        var image = 'img/map_point_tiny.svg';
        /* Static marker image */
        var marker = new google.maps.Marker({
            position: {lat: 40.715536, lng: -73.954927},
            map: map,
            icon: image
        });

        var svg = $('#svgline');
        var modal = $('.map-tooltip');
        var map_layer = $('.layer-pointers');
        /* data */
        var points = [
            {
                'id': 1,
                'title': 'Evo North America',
                'address': '555 California St #4925 <br> San Francisco, CA 94104 <br> United States',
                'email': 'sanfran@evopricing.com',
                'location': {
                    'lat': 37.7816681,
                    'lng': -122.4190305
                },
                'point': {
                    'top': 43.4,
                    'left': 9.3
                }
            }, {
                'id': 2,
                'title': 'Evo Mexico',
                'address': 'Paseo de la Reforma 284, 17º andar <br> Colônia Juárez, Mexico City DF – 06600 <br> Mexico',
                'email': 'mexico.df@evopricing.com',
                'location': {
                    'lat': 19.3990998,
                    'lng': -99.1092886
                },
                'point': {
                    'top': 55,
                    'left': 15
                }
            }, {
                'id': 3,
                'title': 'Evo Italy',
                'address': 'via Luigi Sturzo 17 86039 Termoli (CB) Italy',
                'email': 'torino@evopricing.com',
                'location': {
                    'lat': 41.9969893,
                    'lng': 14.991944
                },
                'point': {
                    'top': 42.2,
                    'left': 49
                }
            },
            {
                'id': 4,
                'title': 'Evo Europe Limited',
                'address': '46 Berwick Street <br> London, W1F 8SG <br> United Kingdom',
                'email': 'london@evopricing.com',
                'location': {
                    'lat': 51.5127802,
                    'lng': -0.1320595
                },
                'point': {
                    'top': 36,
                    'left': 44.8
                }
            }, {
                'id': 5,
                'title': 'Evo France',
                'address': '27/29 rue Bassano <br> Paris, 75008 <br> France',
                'email': 'paris@evopricing.com',
                'location': {
                    'lat': 48.869995,
                    'lng': 2.2987865
                },
                'point': {
                    'top': 38.6,
                    'left': 45.8
                }
            }, {
                'id': 6,
                'title': 'Evo Asia',
                'address': 'Unit 01, 82/F Int’l Commerce Ctr <br> 1 Austin Road West, Kowloon <br> Hong Kong',
                'email': 'hongkong@evopricing.com',
                'location': {
                    'lat': 22.3022922,
                    'lng': 114.1616817
                },
                'point': {
                    'top': 53,
                    'left': 79.6
                }
            }, {
                'id': 7,
                'title': 'Evo Russia',
                'address': 'Moscow Embankment Tower 10 Presnenskaya Naberezhnaya Block C',
                'email': 'moscow@evopricing.com',
                'location': {
                    'lat': 55.7467498,
                    'lng': 37.537627
                },
                'point': {
                    'left': 55.6,
                    'top': 32
                }
            }
        ];

        /*
        *
        *   Search in objects array by ID
        *   
        * */
        function search(n, a) {
            for (var i = 0; i < a.length; i++) if (a[i].id === n) {
                return a[i]
            }
        }

        /*
        *   
        *   Add map content
        *   
        * */
        function addContent(place) {
            $('[data-point="title"]').html(place.title);
            $('[data-point="address"]').html(place.address);
            $('[data-point="email"] a').html(place.email);
            $('[data-point="email"] a').attr('href', 'mailto:' + place.email);
        }

        /*
        *   
        *   Draw line between map and map modal
        *   
        * */
        function drowLine(p, direction) {
            var m = modal.offset();
            var height = p.top - (m.top - $(window).scrollTop());
            var line_shadow = svg.find('#lineShadow');
            var line = svg.find('#line');
            if (direction === 'right') {
                var width = m.left - p.left;
                line.attr('x1', 10);
                line.attr('x2', width);
                line.attr('y1', height - 15);
                line.attr('y2', 135);
                line_shadow.attr('x1', 10);
                line_shadow.attr('x2', width);
                line_shadow.attr('y1', height - 10);
                line_shadow.attr('y2', 142);
            } else if (direction === 'left') {
                var width = p.left - m.left - 300;
                line.attr('x1', 10);
                line.attr('x2', width);
                line.attr('y1', 135);
                line.attr('y2', height - 15);
                line_shadow.attr('x1', 10);
                line_shadow.attr('x2', width - 5);
                line_shadow.attr('y1', 142);
                line_shadow.attr('y2', height - 10);
            }
            svg.attr('width', width);
            svg.attr('height', height);
        }

        /*
        *
        *   Add points on a static page
        *
        * */
        points.forEach(function (p) {
            var html = '<div data-pointer-item="' + p.id + '" class="point-item" style="left:' + p.point.left + '%; top:' + p.point.top + '%"><img src="./img/map_point.svg" class="point-item-img" alt="' + p.title + '"></div>';
            map_layer.append(html);
        });

        /*
        *
        *   On click handler
        *
        * */
        $('[data-pointer-item]').on('click', function () {
            /* reset */
            svg.css('display', 'block');
            modal.removeClass("visible right");

            /* getting id */
            var point = $(this);
            var id = parseInt(point.data('pointer-item'));

            /* calculate modal position */
            var rect = point.offset();
            var point_left = Math.round(rect.left);
            var ww = $(window).width() / 2;

            /* get data */
            var place = search(id, points);

            /* add content */
            addContent(place);

            /* change map and marker location */
            map.setCenter(new google.maps.LatLng(place.location.lat, place.location.lng));
            var marker = new google.maps.Marker({
                position: {lat: place.location.lat, lng: place.location.lng},
                map: map,
                icon: image
            });

            /* show modal depend on position calc. */
            if (point_left > ww) {
                modal.addClass('visible');
                drowLine(rect, 'left');
            } else {
                modal.addClass('visible right');
                drowLine(rect, 'right');
            }
        });

        $(document).on('click', function (event) {
            if (!$(event.target).closest(".map-tooltip, .point-item-img").length) {
                $("body").find(".map-tooltip").removeClass("visible right");
                svg.css('display', 'none');
            }
        });

    } /* END of contact page map scripts */

    /*
    *
    *   Toggle class for .page-header.page-header--tiny
    *
    * */
    var header = $('[data-action="toggle-class-on-scroll"]');
    if ($(window).scrollTop() >= 100) {
        header.addClass('on-scroll');
    } else {
        header.removeClass('on-scroll');
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
            header.addClass('on-scroll');
        } else {
            header.removeClass('on-scroll');
        }
    });

    /*
    *
    *   Scroll to top on click
    *
    * */
    $('[data-action="scroll-top"]').on('click', function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });

    /*
    *
    *   Smooth scroll top ID
    *
    * */
    $('a[href*="#"]')
        .not('[href="#"]')
        .not('[href="#0"]')
        .not('[data-action="scroll-top"]')
        .click(function (event) {
            if (
                location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
                &&
                location.hostname === this.hostname
            ) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                var offset;
                if($(window).width > 768) {
                    offset = 100;
                } else {
                    offset = 70;
                }
                if (target.length) {
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top - offset
                    }, 1000);
                }
            }
        });

});



var ctx = document.getElementById('chart_1');
var options_chart1 = {
    type: 'line',
    data: {
        labels: ["-10", "-8", "-6", "-4", "-2", "0", "2", "4", "6", "8", "10"],
        datasets: [
            {
                label: "Margin",
                backgroundColor: 'rgba(255, 255, 255, 0.18)',
                borderColor: '#FFFFFF',
                data: [1, 1.3, 2, .4, 0.2, 0.5, 1.8, 5, 4, 6, 7]
            },
            {
                label: "Revenues",
                backgroundColor: 'rgba(79, 99, 115, 0.18)',
                borderColor: '#4F6373',
                data: [2, 1, 1.3, .5, 0.8, 1, 4, 7, 7.2, 8, 9]
            },
            {
                label: "Pieces",
                backgroundColor: 'rgba(52, 178, 167, 0.18)',
                borderColor: '#34B2A7',
                data: [0.4, 1.2, 2, 1, 1.2, 1.8, 9, 14, 13, 12, 16]
            }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }
};

var chart = new Chart(ctx, options_chart1);
$('[data-chart1-name]').on('click', function () {
    var btn = $(this);
    var data = chart.config.data;
    var datasets = data.datasets;
    var dataName = btn.data('chart1-name');
    if( btn.hasClass('hide')) {
        btn.removeClass('hide');
        for (var i=0; i < datasets.length; i++) {
            if (datasets[i].label === dataName) {
                datasets[i].hidden = false;
            }
        }
    } else {
        btn.addClass('hide');
        for (var i=0; i < datasets.length; i++) {
            if (datasets[i].label === dataName) {
                datasets[i].hidden = true;
            }
        }
    }
    chart.update();
});




var ctx = document.getElementById('chart_2');
var options_chart2 = {
    type: 'line',
    data: {
        labels: ["wk12", "wk13", "wk14", "wk15", "wk16", "wk17", "wk18", "wk19", "wk20", "wk21"],
        datasets: [
            {
                label: "Lift",
                borderColor: '#E1E1E1',
                fill: false,
                data: [10, 13, 16, 40, 20, 75, 190, 170, 180, 200, 180]
            },
            {
                label: "Control",
                borderColor: '#4F6373',
                fill: false,
                data: [610, 650, 800, 980, 880, 800, 1100, 510, 400, 420]
            },
            {
                label: "Test",
                borderColor: '#34B2A7',
                fill: false,
                data: [630, 680, 900, 1100, 1180, 1200, 1500, 1300, 1200, 1300]
            }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }
};

var chart2 = new Chart(ctx, options_chart2);
$('[data-chart2-name]').on('click', function () {
    var btn = $(this);
    var data = chart2.config.data;
    var datasets = data.datasets;
    var dataName = btn.data('chart2-name');
    if( btn.hasClass('hide')) {
        btn.removeClass('hide');
        for (var i=0; i < datasets.length; i++) {
            if (datasets[i].label === dataName) {
                datasets[i].hidden = false;
            }
        }
    } else {
        btn.addClass('hide');
        for (var i=0; i < datasets.length; i++) {
            if (datasets[i].label === dataName) {
                datasets[i].hidden = true;
            }
        }
    }
    chart2.update();
});
